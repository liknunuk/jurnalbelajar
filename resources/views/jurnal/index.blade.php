@php
    function formatTanggal($tanggal){
        list($t,$b,$h) = explode("-",$tanggal);
        return "$h-$b-$t";
    }
@endphp
@extends('templates.tutor')
@section('konten-utama')
    <div class="row">
        <div class="col">
            <section class="page-title"><h4>Jurnal Hari Ini</h4></section>
            <section class="page-nav">
                <a href="{{ route('jurnal.create') }}" class="btn btn-primary btn-sm">
                    <i class="bi bi-plus-circle"></i> Jurnal
                </a>
            </section>
            <section class="page-content">
                <table class="table table-sm table-bordered">
                    <thead>
                        <tr class="text-center bg-dark text-light">
                            <th>Tanggal</th>
                            <th>Tutor</th>
                            <th>Kelas</th>
                            <th>Pelajaran</th>
                            <th>Topik</th>
                            <th><i class="bi bi-menu-up"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jurnals as $jurnal)
                            <tr>
                                <td>{{ formatTanggal($jurnal->tanggal) }}</td>
                                <td>{{ $jurnal->tutor }}</td>
                                <td class="text-center">{{ $jurnal->kelas }}</td>
                                <td>{{ $jurnal->pelajaran }}</td>
                                <td>{{ $jurnal->topik }}</td>
                                <td class="text-center">
                                    <form class="control" action="{{ route('jurnal.destroy',$jurnal->id) }}" method="post">
                                        <a href="{{ route('jurnal.show',$jurnal->id) }}" class="btn btn-sm btn-success">
                                            <i class="bi bi-list-ol"></i>
                                        </a>
                                        <a href="{{ route('jurnal.edit',$jurnal->id) }}" class="btn btn-sm btn-warning">
                                            <i class="bi bi-pencil-square"></i>
                                        </a>
                                        <a href="{{ url('hadir/'.$jurnal->id) }}" class="btn btn-info btn-sm">
                                            <i class="bi bi-check2-circle"></i>
                                        </a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-danger btn-sm"><i class="bi bi-x-octagon"></i></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
@section('memodalan')
    
@endsection
@section('sesekripan')
    
@endsection
