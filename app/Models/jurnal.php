<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jurnal extends Model
{
    use HasFactory;
    protected $table = 'jurnals';
    protected $primaryKey = "id";
    protected $guarded = ['id'];

    public function presensi(){
        return $this->hasMany(presensi::class, 'id', 'jurnals_id');
    }
}
