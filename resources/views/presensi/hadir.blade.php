@extends('templates.tutor')
@section('konten-utama')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <section class="page-title"><h4>Prensensi</h4></section>
            <section class="page-content">
                <form action="{{ route('presensi.store') }}" method="post">
                    @csrf
                    <div class="form-group row mb-2">
                        <label for="jurnals_id" class="col-md-4">Kode Jurnal</label>
                        <div class="col-md-8">
                            <input type="hidden" id="jurnals_id" name="jurnals_id" value="{{ $jurnal->id }}">
                            <input type="text" id="jurnalID" class="form-control" readonly value="{{ $jurnal->jurnalID }}">
                        </div>
                    </div>
                    <div class="form-group row mb-2">
                        <label for="namaSiswa" class="col-md-4">Nama Siswa</label>
                        <div class="col-md-8">
                            <input type="text" name="namaSiswa" id="namaSiswa" class="form-control">
                        </div>
                    </div>
                    <div class="form-group py-2 justify-content-center">
                        <button type="submit" class="btn btn-primary">Hadir !</button>
                    </div>
                </form>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <div class="table-responsive py-3">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="100">No.</th>
                            <th>Siswa</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $nomor=1;
                        @endphp
                        @foreach ($siswas as $siswa)
                            <tr>
                                <td>{{ $nomor++ }}</td>
                                <td>{{ $siswa->namaSiswa }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('memodalan')
    
@endsection
@section('sesekripan')
    
@endsection