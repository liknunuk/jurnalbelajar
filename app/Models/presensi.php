<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class presensi extends Model
{
    use HasFactory;
    protected $table='presensis';
    protected $guarded=['id'];

    public function jurnal(){
        return $this->belongsTo(jurnal::class, 'jurnals_id', 'id');
    }
}
