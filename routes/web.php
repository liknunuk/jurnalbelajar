<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JurnalController;
use App\Http\Controllers\PresensiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->to('/jurnal');
});

Route::resource('jurnal', JurnalController::class);
Route::resource('presensi', PresensiController::class);
Route::get('hadir/{jurnalID}', [PresensiController::class, 'hadir']);
Route::get('rekap',[PresensiController::class,'rekap']);
