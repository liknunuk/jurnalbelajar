@php
if($jurnal!==null){
    $dokumen = json_decode($jurnal->dokumen,false);
    foreach($dokumen as $dok){
        $materi = $dok->materi;
        $tugas = $dok->tugas;
    }
}else{
        $materi = "Tidak Ada";
        $tugas = "Tidak Ada";
    }
@endphp

@extends('templates.tutor')

@section('konten-utama')
    <div class="row">
        <div class="col-md-6 mx-auto">
            <section class="page-title">
                <h4>Jurnal Hari Ini</h4>
            </section>
            <section class="page-content">
                @if ($jurnal !== null)
                    <form action="{{ route('jurnal.update',$jurnal->id) }}" method="post">  
                    @method('PATCH')
                @else
                    <form action="{{ route('jurnal.store') }}" method="post">                    
                @endif
                @csrf
                <input type="hidden" name="id" value="{{ $jurnal==null ? "" : $jurnal->id }}">
                <div class="mb-1 row">
                    <label for="jurnalID" class="col-md-4 text-right">Kode Jurnal</label>
                    <div class="col-md-8">
                        <input type="text" id="idJurnal" class="form-control" value="{{ $jurnal==null ? $jurnalID : substr($jurnalID,3,5) }}" readonly>
                        <input type="hidden" name="jurnalID" id="jurnalID" value="{{ $jurnal==null ? date('y').'-'.$jurnalID.'-'.sprintf('%03d',date('z')) : $jurnal->jurnalID }}">                        
                    </div>
                </div>
                <div class="mb-1 row">
                    <label for="tanggal" class="col-md-4 text-right">Tanggal</label>
                    <div class="col-md-8">
                        <input type="date" name="tanggal" id="tanggal" class="form-control" value="{{ $jurnal==null ? date('Y-m-d') : $jurnal->tanggal }}">
                    </div>
                </div>
                <div class="mb-1 row">
                    <label for="kelas" class="col-md-4 text-right">Kelas</label>
                    <div class="col-md-8">
                        <select name="kelas" id="kelas" class="form-control">
                            @php
                                $kelas =[
                                    '7'=>'Paket B - Kelas 7',
                                    '8'=>'Paket B - Kelas 8',
                                    '9'=>'Paket B - Kelas 9',
                                    '10'=>'Paket C - Kelas 10',
                                    '11'=>'Paket C - Kelas 11',
                                    '12'=>'Paket C - Kelas 12'
                                ];
                                
                            @endphp
                            @if ($jurnal !== null)
                                <option value="{{ $jurnal->kelas }}">{{ $kelas[$jurnal->kelas] }}</option>
                            @endif
                            @foreach ($kelas as $val=>$lbl)
                                <option value="{{ $val }}">{{ $lbl }}</option>
                            @endforeach
                            
                        </select>
                    </div>
                </div>
                <div class="mb-1 row">
                    <label for="pelajaran" class="col-md-4 text-right">Mata Pelajaran</label>
                    <div class="col-md-8">
                        <input type="text" name="pelajaran" id="pelajaran" class="form-control" value="{{ $jurnal == null ? "" : $jurnal->pelajaran }}">
                    </div>
                </div>
                <div class="mb-1 row">
                    <label for="topik" class="col-md-4 text-right">Topik Bahasan</label>
                    <div class="col-md-8">
                        <input type="text" name="topik" id="topik" class="form-control" value="{{ $jurnal == null ? "" : $jurnal->topik }}">
                    </div>
                </div>
                <div class="mb-1 row">
                    <label for="tutor" class="col-md-4 text-right">Nama Tutor</label>
                    <div class="col-md-8">
                        <input type="text" name="tutor" id="tutor" class="form-control" value="{{ $jurnal == null ? "" : $jurnal->tutor }}">
                    </div>
                </div>
                <div class="mb-1 row">
                    <label for="materi" class="col-md-4 text-right">Link Materi Pembelajaran</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="materi" value="{{ $materi }}">
                    </div>
                </div>
                <div class="mb-1 row">
                    <label for="tugas" class="col-md-4 text-right">Link Pengumpulan Tugas</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="tugas" value="{{ $tugas }}">
                    </div>
                </div>
                <input type="hidden" name="dokumen" id="dokumen">
                <div class="mb-1 container">
                    <div class="row">
                        <div class="col-4 mx-auto">
                            <div class="form-check justify-content-center">
                                <input class="form-check-input" type="checkbox" id="cekForm">
                                <label class="form-check-label" for="cekForm">
                                  Periksa Data
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 mx-auto text-center">
                            <input type="submit" value="Catat Jurnal" id="submitJurnal" class="btn btn-dark btn-sm" style="visibility: hidden;">
                        </div>
                    </div>
                </div>
                
                </form>
            </section>
        </div>
    </div>
@endsection
@section('memodalan')
    
@endsection
@section('sesekripan')
    <script>
        $("#cekForm").click( function(){
            if( $(this).is(':checked')){
                $("#submitJurnal").css('visibility','visible');
                setDokumen();
            }else{
                $("#submitJurnal").css('visibility','hidden');
            }
        })

        function setDokumen(){
            let dokumen={'dokumen':{'materi':$("#materi").val() , 'tugas':$("#tugas").val()}}
            $("#dokumen").val(JSON.stringify(dokumen));
        }
    </script>
@endsection