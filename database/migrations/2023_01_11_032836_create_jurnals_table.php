<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /**
      * JurnalID = year-randomchar-dayOfTheYear
      */
    public function up()
    {
        Schema::create('jurnals', function (Blueprint $table) {
            $table->id();
            $table->string('jurnalID', 12)->unique();
            $table->date('tanggal');
            $table->string('kelas', 2);
            $table->string('pelajaran',50);
            $table->string('topik', 150);
            $table->string('tutor', 50);
            $table->json('dokumen');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnals');
    }
};
