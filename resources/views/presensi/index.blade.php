@extends('templates.tutor')
@section('konten-utama')    
    <div class="row">
        <div class="col">
            <section class="page-title"><h4>Kehadiran Warga Belajar</h4></section>
            <section class="page-content">

                <div class="table-responsive">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>ID Jurnal</th>
                                <th>Tanggal</th>
                                <th>Pelajaran</th>
                                <th>Topik</th>
                                <th>Nama Siswa</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </section>
        </div>
    </div>
@endsection
@section('memodalan')
    
@endsection
@section('sesekripan')
    
@endsection