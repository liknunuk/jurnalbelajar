@extends('templates.tutor')
@section('konten-utama')
    <div class="row">
        <div class="col-lg-4 mx-auto">
            <section class="page-title"><h4>Detil Jurnal</h4></section>
            <section class="page-content">
                <div class="list-group">
                    {{-- jurnalID , tanggal , kelas , pelajaran ,topik, tutor , dokumen --}}
                    <li class="list-group-item py-1">
                        <small><strong>ID Jurnal</strong></small>
                        <span class="float-end mr-3">{{ $jurnal->jurnalID }}</span>
                    </li>
                    <li class="list-group-item py-1">
                        <small><strong>Tanggal pembelajaran</strong></small>
                        <span class="float-end mr-3">{{ $jurnal->tanggal }}</span>
                    </li>
                    <li class="list-group-item py-1">
                        <small><strong>Kelas</strong></small>
                        <span class="float-end mr-3">{{ $jurnal->kelas }}</span>
                    </li>
                    <li class="list-group-item py-1">
                        <small><strong>Mata Pelajaran</strong></small>
                        <span class="float-end mr-3">{{ $jurnal->pelajaran }}</span>
                    </li>
                    <li class="list-group-item py-1">
                        <small><strong>Topik Bahasan</strong></small>
                        <span class="float-end mr-3">{{ $jurnal->topik }}</span>
                    </li>
                    <li class="list-group-item py-1">
                        <small><strong>Tutor</strong></small>
                        <span class="float-end mr-3">{{ $jurnal->tutor }}</span>
                    </li>
                    @php
                        $dokumen = json_decode($jurnal->dokumen);
                    @endphp
                    @foreach ($dokumen as $dok)
                    
                        <li class="list-group-item py-1">
                            <small><strong>Materi</strong></small><br/>
                            <span class="float-end mr-3">{{ $dok->materi }}</span>
                        </li>

                        <li class="list-group-item py-1">
                            <small><strong>Tugas</strong></small><br/>  
                            <span class="float-end mr-3">{{ $dok->tugas }}</span>
                        </li>

                    @endforeach
                </div>
                <div class="text-center">
                    <a href="{{ route('jurnal.index') }}" class="btn btn-sm btn-primary">Kembali</a>
                </div>
            </section>
        </div>
    </div>
@endsection
@section('memodalan')
    
@endsection
@section('sesekripan')
    
@endsection