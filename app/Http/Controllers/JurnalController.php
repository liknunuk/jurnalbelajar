<?php

namespace App\Http\Controllers;

use App\Models\jurnal;
use Illuminate\Http\Request;

class JurnalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jurnals = jurnal::orderBy('id','desc')->paginate(20);
        return view('jurnal.index')->with(['jurnals' => $jurnals]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        $jurnalID = $this->jurnalID();
        return view('jurnal.form')->with(['jurnal'=>$data,'jurnalID'=>$jurnalID]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Jurnal::create($request->all());
        return redirect()->route('jurnal.index')->with('success','Jurnal Tersimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function show(jurnal $jurnal)
    {
        return view('jurnal.detail')->with('jurnal', $jurnal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function edit(jurnal $jurnal)
    {
        $data = $jurnal;
        return view('jurnal.form')->with(['jurnal'=>$data,'jurnalID'=>$jurnal->jurnalID]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jurnal $jurnal)
    {
        $jurnal->id = $request->id;
        $jurnal->jurnalID = $request->jurnalID;
        $jurnal->tanggal = $request->tanggal;
        $jurnal->kelas = $request->kelas;
        $jurnal->pelajaran = $request->pelajaran;
        $jurnal->topik = $request->topik;
        $jurnal->tutor = $request->tutor;
        $jurnal->dokumen = $request->dokumen;
        $jurnal->save();
        return redirect()->route('jurnal.index')->with('success','Jurnal diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function destroy(jurnal $jurnal)
    {
        $jurnal->delete();
        return redirect()->route('jurnal.index')->with('success', 'Jurnal dihapus');
    }

    private function jurnalID(){
        
        $strings = "abcdefghijklmnopqrstuvwxyz";
        $numbers = mt_rand(11,99);
        $stridx1 = mt_rand(0,25);
        $stridx2 = mt_rand(0,25);
        $stridx3 = mt_rand(0,25);

        $string1 = $strings[$stridx1];
        $string2 = $strings[$stridx2];
        $string3 = $strings[$stridx3];

        $randomChar = $string1.$string2.$string3.$numbers;
        return strtoupper($randomChar);
    }
}
